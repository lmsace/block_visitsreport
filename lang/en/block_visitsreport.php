<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * All the strings used in this component.
 *
 * @package   block_visitsreport
 * @copyright Moodle Dev
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['pluginname'] = 'Site visits report';
$string['sitevisits'] = 'Site visits';
$string['mostvisitedcourse'] = 'Most visited courses';
$string['coursevisits'] = 'Course visits';
$string['mostfrequentusers'] = 'Most Frequent users';
$string['visitsreport'] = 'Visits Report';
$string['visits'] = 'Visits';

$string['mostvisitedcourse_heading']     = 'Most visited courses by users';
$string['sitevisits_heading']            = 'Site visits by month';
$string['mostfrequentusers_heading']     = 'List of most frequently visited users.';
$string['coursevisits_heading']          = 'User visits in last 7 days per month';
$string['nodatafound']                   = 'Data not available to generate the report ';
$string['reportnav']                     = '{$a->n} Reports';
$string['visitsreport:myaddinstance']    = 'Add a new visits report block to the page';
$string['visitsreport:viewcoursereport'] = 'View course visits report';
$string['visitsreport:viewsitereport']   = 'View site visits report';
$string['visitsreport:viewusersreport']  = 'View most frequently visited users report';
$string['visitsreport:addinstance']      = 'Add a new visits report block to the page';
$string['timespent']                     = 'Time spent (Hours.Minutes) ';
$string['departmentsiteusage']           = 'Department wise site usage';
$string['departmentsiteusage_heading']   = 'Department wise site usage';
$string['siteuserspent']                 = 'Users time spent by month';
$string['siteuserspent_heading']         = 'Users time spent in site/course';
$string['startdate']                     = 'Start date';
$string['enddate']                       = 'End date';
$string['getreports']                    = 'Get reports';
$string['back']                          = 'Back';
$string['visits_record_file']            = 'Visits record';
$string['most_visited_courses_file']     = 'Most visited courses';
$string['top_departmentusers_file']      = 'Top department users';
$string['timespent_bydepartment_file']   = 'Timespent by department';
$string['course_visits_file']            = 'Course visits';
$string['top_visiters_file']             = 'Top visiters';
$string['users_timespent_file']          = 'Users timespent';
$string['topdepartmentusers']            = 'Top department users';
$string['topdepartmentusers_heading']    = 'Top 20 users in the department';