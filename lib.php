<?php

if (!function_exists('block_visitsreport_time_track')) {

    function block_visitsreport_time_track() {
        global $PAGE, $USER;
        // echo $PAGE->bodyid;
        // $PAGE->requires->js_call_amd('block_visitsreport/timetracking', 'init', ['userid' => $USER->id, 'pageid' => $PAGE->id]);
    }
    block_visitsreport_time_track();
}

function block_visitsreport_output_fragment_getreport($args) {
    global $PAGE;

    $context = $args['context'];
    $url = new \moodle_url('/blocks/visitsreport/view.php');
    $PAGE->set_url($url);
    $filterdata = $args['filterdata'];

    $report = $args['report'];
    $courseid = $args['courseid'];
    $userid = ($args['userid']) ? ($args['userid']) : '';
    $department = ($args['department']) ? ($args['department']) : '';

    if (!empty($report) && $context != '') {
        $visitreport = new \block_visitsreport\report($report, $courseid);
        if (!empty($filterdata)) {
            parse_str($filterdata, $filter);
            $filter = ['startdate' => $filter['startdate'], 'enddate' => $filter['enddate'], 'userid' => $userid];
            $visitreport->set_filter($filter);
        } 
        if (!empty($userid)) {
            $visitreport->set_user($userid);
        }
        if (!empty($department)) {
            $visitreport->set_departmentfilter($department);
        }
        if (method_exists($visitreport, $report)) {
            return $visitreport->$report();
        }
    }
}


class filterform extends \moodleform {

    public function definition() {
        global $CFG, $PAGE;

        $mform = $this->_form;
        $mform->updateAttributes(['class' => 'visits-report-filter']);

        if (isset($this->_customdata['reportname']) && $this->_customdata['reportname'] == 'users_timespent') {
            $users = $options = [];
            if (isset($this->_customdata['courseid']) && !empty($this->_customdata['courseid'])) {
                $users = ['' => get_string('user')];
                $context = context_course::instance($this->_customdata['courseid']);
                $users += get_enrolled_users($context);
                array_walk($users, function(&$user) {
                    $user = fullname($user);
                });

            } else {
                $options = [
                    'ajax' => 'core_search/form-search-user-selector',
                    'noselectionstring' => get_string('user'),
                    'valuehtmlcallback' => function($userid) {
                        $user = core_user::get_user($userid);
                        return fullname($user, has_capability('moodle/site:viewfullnames', context_system::instance()));
                    }
                ];
            }
            // print_r($users);
            $mform->addElement('autocomplete', 'users', '', $users, $options);
            if (isset($this->_customdata['userid'])) {
                $mform->setDefault('users', $this->_customdata['userid']);
            }
        }

        if (isset($this->_customdata['reportname']) && $this->_customdata['reportname'] == 'top_departmentusers') {
            $users = $options = [];
            $departmentusers = block_visitsreport\report::available_departments();
            foreach ($departmentusers as $userid => $user) {
                $department = $user->department;
                $departments[$department] = $department;
            }

            $mform->addElement('autocomplete', 'department', '', $departments);
            if (isset($this->_customdata['department'])) {
                $mform->setDefault('department', $this->_customdata['department']);
            }
        }

        $mform->addElement('date_selector', 'startdate', get_string('startdate', 'block_visitsreport'), array('startyear' => 2015, 'optional' => true));
        $mform->setDefault('startdate', $this->_customdata['startdate']);

        $mform->addElement('date_selector', 'enddate', get_string('enddate', 'block_visitsreport'), array('startyear' => 2015, 'optional' => true));
        $mform->setDefault('enddate', $this->_customdata['enddate']);

        $mform->addElement('hidden', 'report');
        $mform->setDefault('report', $this->_customdata['reportname']);
        $mform->setType('report', PARAM_TEXT);

        $this->add_action_buttons(false, get_string('getreports', 'block_visitsreport'));
    }

    public function validation($data, $files) {
        // If both start and end dates are set end date should be later than the start date.
        if (!empty($coursedata['startdate']) && !empty($coursedata['enddate']) &&
            ($coursedata['enddate'] < $coursedata['startdate'])) {
                $errors['enddate'] = 'enddatebeforestartdate';
        }
        return $errors; 
    }


}  