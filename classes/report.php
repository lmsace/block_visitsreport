<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version information
 *
 * @package   block_visitsreport
 * @copyright Moodle Dev
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace block_visitsreport;

use DateTime;
use html_writer;
use moodle_exception;

require_once($CFG->dirroot. '/lib/tablelib.php');
/**
 * Reports proccess.
 */
class report {

    public $method;

    public $is_timetracking_available = false;

    public $filter = [];

    public $_customdata = ['startdate' => [], 'enddate' => []];

    public $userid = '';

    public $department = '';

    public $download = '';

    /**
     * Constructor.
     *
     * @param string $method
     * @param int|null $courseid
     */
    function __construct($method='site', $courseid=null, $download = false) {
        global $DB;
        $dbman = $DB->get_manager();
        $this->method = $method;
        $this->set_log_table();
        $this->is_timetracking_available = $dbman->table_exists('local_visits_track');
        $this->download = $download;
        $this->courseid = $courseid;
        $this->_customdata['courseid'] = $courseid;
        $this->_customdata['report'] = $method;
    }

    function set_filter($filter) {
        $this->_customdata['startdate'] = $filter['startdate'] ?: [];
        $this->_customdata['enddate'] = $filter['enddate'] ?: [];


        if (!empty($filter) && isset($filter['startdate']) ) {
            array_walk($filter, function(&$value, $key) {
                if ( !isset($value['day']) ) {
                    return;
                }
                $date = $value['day'].'-'.$value['month'].'-'.$value['year'];
                $date = new DateTime($date);
                $value = $date;
            });
            $filter['start_timestamp'] = $filter['startdate']->getTimeStamp();
            $filter['end_timestamp'] = strtotime("tomorrow", $filter['enddate']->getTimeStamp()) - 1;

        } else {
            $filter = [];
        }
        $this->filter = $filter;
    }

    function set_user($userid) {
        $this->userid = $userid;
        $this->_customdata['userid'] = $userid ?: '';
    }

    function set_departmentfilter($department) {
        $this->department = $department;
        $this->_customdata['department'] = $department ?: '';
    }
    /**
     * Fetch the report content based on the report method.
     *
     * @return void
     */
    public function display() {
        global $PAGE;
        if ($this->method == 'users') {
            $output = $this->top_visiters();
            $output .= $this->top_departmentusers();
            if ($this->is_timetracking_available) {
                $output .= $this->users_timespent();
            }
        } else if ($this->method == 'course') {
            $output = $this->course_visits($this->courseid);
        } else {
            $output = $this->site_reports();
        }
        $PAGE->requires->data_for_js('visitspagedata', ['courseid' => $this->courseid]);
        $PAGE->requires->js('/blocks/visitsreport/js/visits.js');
        return $output;
    }

    /**
     * Sql queries to find the admin user to remove from the report data fetch.
     *
     * @return array Sql and Parameters.
     */
    public function find_adminuser() {
        global $CFG, $DB;
        $siteadmins = explode(',', $CFG->siteadmins);
        list($insql, $inparams) = $DB->get_in_or_equal($siteadmins, SQL_PARAMS_NAMED, '', false);
        return [ $insql, $inparams];
    }

    /**
     * Find the global log table to fetch data from.
     *
     * @return void
     */
    public function set_log_table() {

		$logmanager = get_log_manager();
	    $readers = $logmanager->get_readers();
	  	$reader = reset($readers);
		$this->logtable = $reader->get_internal_log_table_name();
		$this->maxseconds = 150 * 3600 * 24;
	}

    /**
     * Get list of last 12 months from today. It returns the array of timestamp for each month first date.
     *
     * @return array List of last months.
     */
    public function get_last_months() {
        $endtime = (isset($this->filter['end_timestamp']) && !empty($this->filter['end_timestamp'])) ? $this->filter['end_timestamp'] : time();
        $month = $endtime;
        // Find the month counts between start and end dates filter.
        $howmanymonths = 12;
        if (!empty($this->filter) && $this->filter['startdate'] instanceof DateTime) {
            $diffmonths = $this->filter['enddate']->diff($this->filter['startdate']);
            if (isset($diffmonths->m)) {
                $howmanymonths = (($diffmonths->y) * 12) + ($diffmonths->m);
            }
        }

        $months[0] =  strtotime('tomorrow', strtotime( date('r', $endtime) )) -1;
        for ($i = 1; $i <= $howmanymonths; $i++) {
            $month = strtotime('last month', $month);
            $months[] = strtotime(date("r", $month));
        }
        if (count($months) == 1) {
            $months[] = $this->filter['start_timestamp'];
        }
        return $months;
    }


    /**
     * Site visits reports available report charts are listed here.
     *
     * @return string Report of site visits and most visited courses.
     */
    public function site_reports() {
        $report = $this->visits_record();
        $report .= $this->most_visited_courses();
       
        if ($this->is_timetracking_available) {
            $report .= $this->timespent_bydepartment();
        }
        return $report;
    }

    /**
     * List of visits in the site per month report data fetching from logs table.
     *
     * @return string Chart content of the report.
     */
    public function visits_record() {
        global $DB, $OUTPUT;

        $months = $this->get_last_months();
        list($insql, $inparams) = $this->find_adminuser();
        $sql = "SELECT MONTH(FROM_UNIXTIME(`timecreated`)) as createdmonth, timecreated, COUNT(*) as visits
            FROM {".$this->logtable."} WHERE
            timecreated BETWEEN :timestart and :timeend
            AND action='viewed'
            AND userid ".$insql."
            GROUP BY MONTH(FROM_UNIXTIME(`timecreated`))
        ";
        $params = [
            'timestart' => end($months),
            'timeend' => reset($months)
        ];
        if (!empty($this->filter)) {
            $params = ['timestart' => $this->filter['start_timestamp'], 'timeend' => $this->filter['end_timestamp']];
        }

        $report = $DB->get_records_sql($sql, $params + $inparams );

        $ylabel = get_string('visits', 'block_visitsreport');
        $visits  = (!empty($report)) ? $this->chart_series($ylabel, array_column($report, 'visits')) : [];
        $labels = array_column($report, 'timecreated');
        array_walk($labels, function(&$value) {
            $value = date('M Y', $value);
        });

        $xlabel = get_string('month');
        $title = get_string('sitevisits', 'block_visitsreport');
        $heading = get_string('sitevisits_heading', 'block_visitsreport');

        // Table count sql.
        $countsql = "SELECT COUNT(*) 
        FROM {".$this->logtable."} WHERE
        timecreated BETWEEN :timestart and :timeend
        AND action='viewed'
        AND userid ".$insql."
        GROUP BY MONTH(FROM_UNIXTIME(`timecreated`))";


        $reporttable = $this->visitsreport_table('visits_record', $report, $countsql, ['timecreated', 'visits'], [$xlabel, $ylabel]);
        return $this->generate_chart($visits, $labels, 'visits_record', false, $title, $heading, $xlabel, $ylabel, $reporttable);

    }

    /**
     * Most visited courses reports data fetched and processed to the reports.
     *
     * @return string
     */
    public function most_visited_courses() {
        global $DB;

        list($insql, $inparams) = $this->find_adminuser();

        $filtersql = '';
        if (!empty($this->filter)) {
            $filtersql = ' AND l.timecreated BETWEEN :startdate AND :enddate ';
            $inparams += ['startdate' => $this->filter['start_timestamp'], 'enddate' => $this->filter['end_timestamp']];
        }

        $sql = 'SELECT l.courseid as id, l.courseid, c.fullname, count(*) as visits FROM {'.$this->logtable.'} l
        INNER JOIN {course} c on c.id = l.courseid
        WHERE l.courseid > 1 AND l.userid '.$insql.' '. $filtersql .' GROUP BY l.courseid ORDER BY visits DESC ';
        $records = $DB->get_records_sql($sql, $inparams);

        $xlabel = get_string('visits', 'block_visitsreport');
        $visits = (!empty($records)) ? $this->chart_series($xlabel, array_column($records, 'visits')) : [];
        $labels = array_column($records, 'fullname');

        $ylabel = get_string('course');
        $title = get_string('mostvisitedcourse', 'block_visitsreport');
        $heading = get_string('mostvisitedcourse_heading', 'block_visitsreport');

        $reporttable = $this->visitsreport_table('most_visited_courses', $records, '', ['fullname', 'visits'], [$ylabel, $xlabel]);
        return $this->generate_chart($visits, $labels, 'most_visited_courses', true, $title, $heading, $xlabel, $ylabel, $reporttable);
    }

    /**
     * List of available user departments.
     */
    public static function available_departments() {
        global $DB;

        $sql = 'SELECT id, department FROM {user} u GROUP BY department';
        $data = $DB->get_records_sql($sql, []);
        return $data;
    }

    /**
     * Chart of top 20 users from selected department.
     *
     * @return string
     */
    public function top_departmentusers() {
        global $DB;

        $params = [];

        list($insql, $inparams) = $this->find_adminuser();
        $filtersql = '';
        if (!empty($this->filter)) {
            $filtersql = ' AND l.timecreated BETWEEN :startdate AND :enddate ';
            $inparams += ['startdate' => $this->filter['start_timestamp'], 'enddate' => $this->filter['end_timestamp']];
        }

        $params['department'] = (isset($this->department)) ? $this->department : '';

        $sql = "SELECT u.id, count(*) as visits, u.department, u.firstname, u.lastname FROM {".$this->logtable."} l
        LEFT JOIN {user} u  ON u.id = l.userid
        WHERE u.deleted = 0 AND u.suspended = 0 AND u.confirmed = 1 AND u.department = :department
        AND l.action='viewed' ";
        if ($this->courseid) {
            $sql .= ' AND l.courseid=:courseid ';
            $params['courseid'] = $this->courseid;
        }
        $sql .= $filtersql;
        $sql .= 'GROUP BY l.userid ORDER BY visits DESC';

        $users = $DB->get_records_sql($sql, $params + $inparams, 0, 20);

        $params;
        $xlabel = get_string('visits', 'block_visitsreport');
        $visits = $this->chart_series($xlabel, array_column($users, 'visits'));
        $labels = [];
        array_walk($users, function($value) use (&$labels) {
            $labels[] = $value->firstname.' '.$value->lastname. ' ('.$value->visits.') ';
        });

        $ylabel = get_string('users');
        $title = get_string('topdepartmentusers', 'block_visitsreport');
        $heading = get_string('topdepartmentusers_heading', 'block_visitsreport');

        $reporttable = $this->visitsreport_table('top_departmentusers', $users, '', ['userfullname', 'visits'], [$ylabel, $xlabel]);
        return $this->generate_chart($visits, $labels, 'top_departmentusers', true, $title, $heading, $xlabel, $ylabel, $reporttable);
    }

    /**
     * Report of department users timespent in site.
     *
     * @return string
     */
    public function timespent_bydepartment() {
        global $DB, $PAGE;
        // $departments = $this->get_userdepartments();
        $params = $visits = [];
        $filtersql = '';
        if (!empty($this->filter)) {
            $filtersql = ' AND vt.timecreated BETWEEN :startdate AND :enddate ';
            $params = ['startdate' => $this->filter['start_timestamp'], 'enddate' => $this->filter['end_timestamp']];
        }

        $sql = "SELECT u.id, u.department, SUM(vt.timespent) AS dpart_timespent, count(*) AS userscount FROM {user} u
        LEFT JOIN {local_visits_track} vt ON vt.userid = u.id
        WHERE u.department <> '' ".$filtersql." GROUP BY department ";

        $dpartusers = $DB->get_records_sql($sql, $params);
        $xlabel = get_string('timespent', 'block_visitsreport');
        $timespent = array_column($dpartusers, 'dpart_timespent');
        if (!empty($timespent)) {
            array_walk($timespent, function(&$value) {
                if (!empty($value)) {
                    $value = (int) $value;
                    $zero    = new \DateTime("@0");
                    $offset  = new \DateTime("@$value");
                    $diff    = $zero->diff($offset)->format('%H.%I');
                    $value = $diff;
                }
            });

            $visits = $this->chart_series($xlabel, $timespent);
        }
        $labels = array_column($dpartusers, 'department');

        $ylabel = get_string('department');
        $title = get_string('departmentsiteusage', 'block_visitsreport');
        $heading = get_string('departmentsiteusage_heading', 'block_visitsreport');

        if (count($labels) > 20) {
            $height = (count($labels)) * 20;
            $maxheight = (count($labels) + 10 ) * 20;
            $PAGE->requires->js_amd_inline("require(['jquery', 'core/chartjs'], function($, ChartJS) {
                var height = ".$height.";
                var maxheight = ".$maxheight.";
                $(window).on('load', function() {
                     Chart.helpers.each(Chart.instances, function(instance) {
                        var id = $(instance.canvas).parents('.visits-report-chart').attr('id');
                        if (id == 'timespent_bydepartment') {
                            instance.config.options.responsive = true;
                            instance.config.options.maintainAspectRatio = false;
                            instance.canvas.parentNode.style.height= '".$height."px';
                            instance.update();
                            instance.resize();
                        }
                    })
                })
            })");
            echo '<style>
            .visits-report-chart#timespent_bydepartment .chart-block {
                position:relative; display:block;
            }
            .visits-report-chart#timespent_bydepartment .chart-block-parent  {
                overflow-y: auto;
            }
            #timespent_bydepartment .chart-block {
                height: auto;
            }
            </style>';
        }

        $reporttable = $this->visitsreport_table('timespent_bydepartment', $dpartusers, '', ['department', 'dpart_timespent'], [$ylabel, $xlabel]);
        return $this->generate_chart($visits, $labels, 'timespent_bydepartment', true, $title, $heading, $xlabel, $ylabel, $reporttable);
    }

    public function get_userdepartments(): array {
        global $DB;
        $departments = $DB->get_fieldset_sql("SELECT department FROM {user} WHERE department <> '' GROUP BY department ORDER BY department ASC", []);
        return $departments;
    }
    /**
     * List all course visits report data process.
     *
     * @param int $courseid Id of the course to generate the report.
     * @return string
     */
    public function course_visits() {
        global $DB;
        if (empty($this->courseid)) {
            throw new moodle_exception('unspecifycourseid');
        }
        list($insql, $inparams) = $this->find_adminuser();

        $filtersql = '';
        if (!empty($this->filter)) {
            $filtersql = ' AND timecreated BETWEEN :startdate AND :enddate ';
            $inparams += ['startdate' => $this->filter['start_timestamp'], 'enddate' => $this->filter['end_timestamp']];
        }


        $sql = 'SELECT * FROM {'.$this->logtable.'} WHERE action="viewed" AND userid '.$insql.' AND courseid=:courseid ';
        $params = ['courseid' => $this->courseid];
        $conditionsql = $this->generate_timestamp($params);
        $sql .= ' AND (' . $conditionsql.')';
        $sql .= $filtersql;

        $records = $DB->get_records_sql($sql, $params + $inparams);
        // Filter unique logins for the day.
        $ylabel = get_string('visits', 'block_visitsreport');
        $visits = $visitseries = [];
        if (!empty($records)) {
            foreach ($records as $key => $record) {
                $month = date('M Y', $record->timecreated);
                $visits[$month] = (array_key_exists($month, $visits)) ? $visits[$month] + 1 : 1;
            }
            $visitseries = $this->chart_series($ylabel, array_values($visits));
        }
        $labels = array_keys($visits);

        array_walk($visits, function(&$visit, $month) {
            $visit = ['month' => $month, 'visits' => $visit];
        });

        $xlabel = get_string('month');
        $title = get_string('coursevisits', 'block_visitsreport');
        $heading = get_string('coursevisits_heading', 'block_visitsreport');

        $reporttable = $this->visitsreport_table('course_visits', $visits, '', ['month', 'visits'], [$xlabel, $ylabel]);
        return  $this->generate_chart($visitseries, $labels, 'course_visits', false, $title, $heading, $xlabel, $ylabel, $reporttable);
    }

    /**
     * Generate the timestamp to get the last 7 days of each month.
     *
     * @param array $params
     * @return string sql condition queries.
     */
    public function generate_timestamp(&$params) {
        $month = time();
        $condition = [];
        for ($i = 1; $i <= 12; $i++) {
            $month = strtotime('last month', $month);
            $prevmonth = strtotime(date('y-m-t', $month));
            $monthend = $prevmonth;
            $startdate = strtotime('-7 days', $prevmonth);
            $startlabel = 'startdate_'.$i;
            $endlabel = 'enddate_'.$i;
            $condition[] = ' timecreated BETWEEN :'.$startlabel.' AND :'.$endlabel;
            $params[$startlabel] = $startdate;
            $params[$endlabel] = $monthend;
        }
        return implode(' OR ', $condition);
    }

    /**
     * Reports based on the most frequently accessed users in the site.
     *
     * @return string
     */
    public function top_visiters() {
        global $DB;

        list($insql, $inparams) = $this->find_adminuser();
        $filtersql = '';
        if (!empty($this->filter)) {
            $filtersql = ' AND l.timecreated BETWEEN :startdate AND :enddate ';
            $inparams += ['startdate' => $this->filter['start_timestamp'], 'enddate' => $this->filter['end_timestamp']];
        }

        $params = [];
        $sql = 'SELECT l.userid, u.firstname, u.lastname, count(*) as visits FROM {'.$this->logtable.'} l
        INNER JOIN {user} u ON u.id = l.userid
        WHERE l.userid '.$insql;
        if ($this->courseid) {
            $sql .= ' AND courseid=:courseid ';
            $params['courseid'] = $this->courseid;
        }
        $sql .= $filtersql;

        $sql .= 'GROUP BY l.userid ORDER BY visits DESC';

        $xlabel = get_string('visits', 'block_visitsreport');
        $records = $DB->get_records_sql($sql, $params + $inparams, 0, 20);
        $visits = (!empty($records)) ? $this->chart_series($xlabel, array_column($records, 'visits')) : [];
        $labels = [];
        array_walk($records, function($value) use (&$labels) {
            $labels[] = $value->firstname.' '.$value->lastname. ' ('.$value->visits.') ';
        });

        $ylabel = get_string('users');
        $title = get_string('mostfrequentusers', 'block_visitsreport');
        $heading = get_string('mostfrequentusers_heading', 'block_visitsreport');

        $reporttable = $this->visitsreport_table('top_visiters', $records, '', ['userfullname', 'visits'], [$xlabel, $ylabel]);
        return $this->generate_chart($visits, $labels, 'top_visiters', true, $title, $heading, $xlabel, $ylabel, $reporttable);
    }

    /**
     * Users timespent in course.
     *
     * @return void
     */
    public function users_timespent() {
        global $DB, $PAGE;
        list($insql, $inparams) = $this->find_adminuser();
        $params = [];
        $filtersql = '';
        if (!empty($this->filter)) {
            $filtersql = ' AND lvt.timecreated BETWEEN :timestart and :timeend ';
            $params = ['timestart' => $this->filter['start_timestamp'], 'timeend' => $this->filter['end_timestamp']];
        }

        if (isset($this->userid) && !empty($this->userid) && !$this->courseid ) {

            $sql = "SELECT lvt.courseid, c.fullname, lvt.timecreated, SUM(lvt.timespent) as timespent, count(*) AS usersessions
            FROM {local_visits_track} lvt
            INNER JOIN {course} c ON c.id = lvt.courseid
            WHERE lvt.userid = :userid
            AND lvt.userid ".$insql;
            $sql .= $filtersql;
            $sql .= " GROUP BY lvt.courseid ORDER BY timespent DESC";

            $params['userid'] = $this->userid;

            $labelfunction = (function($value) use (&$labels) {
                $labels[] = $value->fullname;
            });
            $ylabel = get_string('courses');
            $yfield = 'fullname';
        } else {

            $sql = "SELECT lvt.userid, u.firstname, u.lastname, lvt.timecreated, SUM(lvt.timespent) as timespent, count(*) AS usersessions
                FROM {local_visits_track} lvt
                INNER JOIN {user} u ON u.id = lvt.userid
                WHERE lvt.userid ".$insql;

            $sql .= $filtersql;
            if ($this->courseid) {
                $sql .= ' AND courseid=:courseid ';
                $params['courseid'] = $this->courseid;
            }

            if (!empty($this->userid)) {
                $sql .= ' AND lvt.userid = :userid ';
                $params['userid'] = $this->userid;
            }
            $sql .= " GROUP BY lvt.userid ORDER BY timespent DESC";

            $labelfunction = (function($value) use (&$labels) {
                $labels[] = $value->firstname.' '.$value->lastname;
            });
            $ylabel = get_string('users');
            $yfield = 'userfullname';
        }

        $records = $DB->get_records_sql($sql, $params + $inparams, 0, 20 );

        // Convert the seconds to minutes.
        $visits = [];
        $timespent = array_column($records, 'timespent');
        if (!empty($timespent)) {
            array_walk($timespent, function(&$value) {
                if (!empty($value)) {
                    $value = (int) $value;
                    $zero    = new \DateTime("@0");
                    $offset  = new \DateTime("@$value");
                    $diff    = $zero->diff($offset)->format('%H.%I');
                    $value = $diff;
                }
            });
            $visits  = new \core\chart_series($ylabel, $timespent);
        }

        $labels = [];
        array_walk($records, $labelfunction);

        $xlabel = get_string('timespent', 'block_visitsreport');
        $title = get_string('siteuserspent', 'block_visitsreport');
        $heading = get_string('siteuserspent_heading', 'block_visitsreport');

        $reporttable = $this->visitsreport_table('users_timespent', $records, '', [$yfield, 'user_timespent'], [$ylabel, $xlabel]);
        return $this->generate_chart($visits, $labels, 'users_timespent', true, $title, $heading, $xlabel, $ylabel, $reporttable);
    }

    /**
     * Convert the set of data to chart series.
     *
     * @param string $name
     * @param array $data
     * @return void
     */
    public function chart_series($name, $data) {
        if (empty($data)) {
            return null;
        }
        return new \core\chart_series($name, $data);
    }

    /**
     * Generate the visits chart using moodle chart functions.
     *
     * @param \core\chart_series $series
     * @param array $labels
     * @param boolean $horizontal
     * @param string $title
     * @param string $heading
     * @param string $xlabel
     * @param string $ylabel
     * @return string Content report page.
     */
    public function generate_chart($series, $labels, $name, $horizontal=false, $title="", $heading='', $xlabel='', $ylabel='', $table='') {
        global $OUTPUT, $SESSION;

        // Custom date filter.
        $this->_customdata['reportname'] = $name;
        $this->_customdata['courseid'] = $this->courseid;
        $form = new \filterform(null, $this->_customdata);

        if ($series instanceof \core\chart_series) {
            $chart = new \core\chart_bar();
            if ($horizontal) {
                $chart->set_horizontal(true);
            }
            $chart->add_series($series);
            $chart->set_labels($labels);
            if ($title) {
                $chart->set_title($title);
            }
            if ($xlabel) {
                $chart->get_xaxis(0, true)->set_label($xlabel);
            }
            if ($ylabel) {
                $chart->get_yaxis(0, true)->set_label($ylabel);
            }


            $chart->set_legend_options(
                [
                    'responsive'=> true,
                    'maintainAspectRatio' => false
                ]
            );
        }
        $returnurl = optional_param('returnurl', '', PARAM_URL);
        if (empty($returnurl)) {
            $returnurl = isset($SESSION->wantsurl) ? $SESSION->wantsurl : '';
        }

        $html = html_writer::start_div('visits-report-chart', ['id' => $name]);
        $html .= html_writer::start_div('top-section');

        $html .= html_writer::start_div('heading-block');
        $itag = html_writer::tag('i', '', ['class' => 'fa fa-arrow-left']);
        if ($returnurl) {
            $html .= html_writer::link($returnurl, html_writer::tag('span', $itag . get_string('back', 'block_visitsreport'), ['class' => 'pull-left btn btn-primary']) );
        } else {
            $html .= html_writer::tag('span', $itag.get_string('back', 'block_visitsreport'), ['class' => 'pull-left btn btn-primary', 'onclick' =>'window.history.back()']);
        }
        $html .= html_writer::tag('h3', $heading);
        $html .= html_writer::end_div();

        $html .= html_writer::start_div('right-menu');
        $html .= $form->render();
        $html .= html_writer::end_div();

        $html .= html_writer::start_div('download-menu');
        $html .= $table;
        $html .= html_writer::end_div();

        $html .= html_writer::end_div(); // E.O Top-section.

        $html .= html_writer::start_div('chart-block-parent');
        $html .= html_writer::start_div('chart-block');

        if (isset($chart)) {
            $html .= $OUTPUT->render($chart);
        } else {
            $html .= html_writer::start_div('alert alert-info alert-block fade in report-not-available');
            $html .= html_writer::tag('p', get_string('nodatafound', 'block_visitsreport'), ['class' => 'report-data-not']);
            $html .= html_writer::end_div();
        }
        $html .= html_writer::end_div();
        $html .= html_writer::end_div();



        $html .= html_writer::end_div();

        return $html;
    }

    public function visitsreport_table($reportname, $data, $countsql, $columns, $headers) {
        global $PAGE;

        $tableparams = [];// http_build_query($this->_customdata);
        // $this->_customdata['startdate'] = ['date' => 'tt', 'month' => 'dd'];
        array_walk($this->_customdata, function($val, $key) use (&$tableparams){
            if (is_array($val)) {
                foreach($val as $k => $v) {
                    $param = $key.'['.$k.']';
                    $tableparams[$param] = $v;
                }
            } else {
                $tableparams[$key] = $val;
            }
        });

        $tableparams['reportname'] = $reportname;

        $url = new \moodle_url($PAGE->url, $tableparams);
        $reporttable = new \block_visitsreport\report_table($reportname);
        $reporttable->define_baseurl($url);
        $reporttable->set_report($reportname, $data, $countsql, $columns, $headers);
        $reportfilename = get_string($reportname.'_file', 'block_visitsreport');
        if ($this->download) {
            $reporttable->is_downloading($this->download, $reportfilename);
            $reporttable->out(0, true, true);
        } else {
            ob_start();
            $reporttable->out(0, true, true);
            $table = ob_get_contents();
            ob_clean();
            return $table;
        }
    }
}
