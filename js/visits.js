require(['jquery', "core/fragment", 'core/templates', 'core/yui', 'core/loadingicon'], function($, Fragment, Templates, Y, Loadingicon) {

    "use strict"
    
    var contextid = document.getElementById('contextid').value;

    var filterSubmited = [];

    document.querySelectorAll('form.visits-report-filter').forEach(element => {
        setupEvent(element);
    });


    document.querySelectorAll('form.visits-report-filter select[name="users"]').forEach(element => {
        UserReportEvent(element);
    });

    document.querySelectorAll('form.visits-report-filter select[name="department"]').forEach(element => {
        UserReportEvent(element, 'department');
    });

    document.querySelectorAll('form.dataformatselector').forEach(element => {
        element.setAttribute('target', '_blank');
    });

   /*  document.querySelectorAll('input[type="checkbox"].form-check-input').forEach(element => {
        element.form
    }) */
    $('body').delegate('input[type="checkbox"][name="enddate[enabled]"]', 'click', function() {
        $(this).closest('form').find('input[type="checkbox"][name="startdate[enabled]"]').trigger('click');
    }) 

/*     $('.visits-report-chart input[type="checkbox"]:not([name="startdate[enabled]"])').show();
    $('.visits-report-chart [name="startdate[enabled]"]').hide(); */

    function setupEvent(element) {
        element.addEventListener('submit', (event) => {
            event.preventDefault();
            var formdata = new FormData(element);
            var filterdata = new URLSearchParams(formdata).toString();

            var report = formdata.get('report');
            var userid = formdata.get('users');
            var department = formdata.get('department');
            var courseid = visitspagedata.courseid;
            
            loadReport({'filterdata': filterdata, 'report':  report, 'courseid' : courseid, 'userid': userid, 'department': department});
            // Set filter intialized.
            filterSubmited[report] = true;
        })
    }

    function UserReportEvent(selector, field='users') {

        selector.addEventListener('change', (event) => {

            var formdata = new FormData(selector.form);
            var filterdata = new URLSearchParams(formdata).toString();
            var report = formdata.get('report');
            var courseid = visitspagedata.courseid;

            var params = {'report':  report, 'courseid' : courseid};
            if (field == 'department') {
                var department = formdata.get('department');
                params['department'] = department;
            } else {
                var userid = formdata.get('users');
                params['userid'] = userid;
            }

            if (report in filterSubmited) {
                params['filterdata'] = filterdata;
            }
            loadReport(params, field);
        })
    }

    function loadReport(params, field='') {
        var loadIconElement = '#' + params.report + ' '+ ' form.visits-report-filter';
        Loadingicon.addIconToContainer(loadIconElement);
        Fragment.loadFragment('block_visitsreport', 'getreport', contextid, params).done((html, js) => {
            var newElements = Templates.replaceNode('#'+params.report, html, js);
            if (newElements) {
                console.log(('#' + params.report + ' form.visits-report-filter'));
                var element = document.querySelectorAll('#' + params.report + ' '+ ' form.visits-report-filter')[0];
                setupEvent(element);
                var selector = document.querySelectorAll('#' + params.report + ' '+ ' form.visits-report-filter select[name="users"]');
                console.log(selector.length);
                if (selector.length != 0) {
                    UserReportEvent(selector[0], 'users');
                }

                var selector = document.querySelectorAll('#' + params.report + ' '+ ' form.visits-report-filter select[name="department"]');
                console.log(selector.length);
                if (selector.length != 0) {
                    UserReportEvent(selector[0], 'department');
                }
                
                document.querySelectorAll('form.dataformatselector').forEach(element => {
                    element.setAttribute('target', '_blank');
                });
            }

        });
    }

});